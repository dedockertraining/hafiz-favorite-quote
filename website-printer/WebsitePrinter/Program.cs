﻿using System;
using System.Net;
using System.Threading;

namespace WebsitePrinter
{
    class Program
    {
        static void Main(string[] args)
        {
            string url = Environment.GetEnvironmentVariable("WebsiteUrl");

            if (String.IsNullOrWhiteSpace(url))
            {
                return;
            }

            Console.WriteLine($"Website monitor URL is {url}");

            using (WebClient client = new WebClient())
            {
                try
                {
                    int counter = 0;
                    while (true)
                    {
                        counter++;
                        Console.WriteLine($"Monitor Round {counter}:{Environment.NewLine}{client.DownloadString(url)}");
                        Thread.Sleep(5000);
                    }

                }
                catch (WebException ex)
                {
                    Console.WriteLine("Monitoring failed: " + ex.Message);
                }
            }
        }
    }
}
